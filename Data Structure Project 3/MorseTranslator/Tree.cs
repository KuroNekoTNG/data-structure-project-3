﻿using System;

namespace DataStructureProject3.MorseTranslator {

	/// <summary>
	/// Node class
	/// </summary>
	public class Node {

		public char letter;

		public Node left, right;

		public Node( char letter ) {

			this.letter = letter;

			right = null;

			left = null;

		}

	}

	public class Tree {

		public static Tree Singleton {

			get {

				if ( singleton is null ) { // Checks to see if singleton is null.

					singleton = new Tree(); // Creates a new Tree and assigns it to a singleton.

				}

				return singleton; // Return singleton.

			}

		}

		private static Tree singleton; // The singleton.

		private Node root; // The root node.

		/// <summary>
		/// Adds a node to the tree recursively.
		/// </summary>
		/// <returns>The node recursive.</returns>
		/// <param name="current">Current.</param>
		/// <param name="letter">Letter.</param>
		/// <param name="morseCode">Morse code.</param>
		public Node AddNodeRecursive( Node current, char letter, string morseCode ) {

			if ( current == null ) { // Checks to see if the current node is null.

				return new Node( letter ); // Adds node to the tree if true.

			}

			if ( !string.IsNullOrWhiteSpace( morseCode ) && morseCode[0] == '.' ) { // Checks to see if the morseCode string is empty and the first character of the string is the '.' character.

				if ( morseCode.Length == 1 ) { // Checks to see if the length of the morseCode string is 1.

					morseCode = ""; // If it is the morse code string is cleared.

				} else {

					morseCode = morseCode.Substring( 1 ); // If not the first character of the morseCode string is removed.

				}

				current.left = AddNodeRecursive( current.left, letter, morseCode ); // Calls the method itself with current.left, letter, and morseCode string as input.

			} else if ( !string.IsNullOrWhiteSpace( morseCode ) && morseCode[0] == '_' ) { // Checks to see if the morseCode string is empty and the first character of the string is the '_' character.

				if ( morseCode.Length == 1 ) { // Checks to see if the length of the morseCode string is 1.

					morseCode = ""; // If it is the morse code string is cleared.

				} else {

					morseCode = morseCode.Substring( 1 ); // If not the first character of the morseCode string is removed.

				}

				current.right = AddNodeRecursive( current.right, letter, morseCode ); // Calls the method itself with current.right, letter, and morseCode string as input.

			} else {

				return current; // Returns current node.

			}

			return current; // Returns current node.

		}

		/// <summary>
		/// Wraper function of the AddNodeRecursive method.
		/// </summary>
		/// <param name="letter">Letter.</param>
		/// <param name="morseCode">Morse code.</param>
		public void Add( char letter, string morseCode ) {

			root = AddNodeRecursive( root, letter, morseCode ); // Calls the AddNodeRecursive method with the root, letter, and morseCode as input and assigns that value to the root.

		}

		/// <summary>
		/// Gets the node letter value with inputed morse code.
		/// </summary>
		/// <returns>The node.</returns>
		/// <param name="current">Current.</param>
		/// <param name="morseCode">Morse code.</param>
		public char GetNode( Node current, string morseCode ) {

			if ( current == null ) { // Determines if the current node is null.

				throw new Exception( "Invalid morse code input" ); // Throws exception if true.

			} else if ( !string.IsNullOrWhiteSpace( morseCode ) && morseCode[0] == '.' ) { // Checks to see if the morseCode string is empty and the first character of the string is the '.' character.

				if ( morseCode.Length == 1 ) { // Checks to see if the length of the morseCode string is 1.

					morseCode = ""; // If it is the morse code string is cleared.

				} else {

					morseCode = morseCode.Substring( 1 ); // If not the first character of the morseCode string is removed.

				}

				return GetNode( current.left, morseCode ); // Calls the method itself with current.left and morseCode as input.
			} else if ( !string.IsNullOrWhiteSpace( morseCode ) && morseCode[0] == '_' ) { // Checks to see if the morseCode string is empty and the first character of the string is the '_' character.

				if ( morseCode.Length == 1 ) { // Checks to see if the length of the morseCode string is 1.

					morseCode = ""; // If it is the morse code string is cleared.

				} else {

					morseCode = morseCode.Substring( 1 ); // If not the first character of the morseCode string is removed.

				}

				return GetNode( current.right, morseCode ); // Calls the method itself with current.right and morseCode as input.

			} else {

				return current.letter; // Returns the letter string.

			}

		}

		/// <summary>
		/// Overload function for the GetNode method.
		/// </summary>
		/// <returns>The node.</returns>
		/// <param name="morseCode">Morse code.</param>
		public char GetNode( string morseCode ) {

			return GetNode( root, morseCode ); // Calls the GetNode method with root and morseCode as input.

		}

		/// <summary>
		/// Prints the tree in pre order traversal.
		/// </summary>
		/// <param name="current">Current.</param>
		public void PrintPreOrder( Node current ) {

			if ( current != null ) { // Determines if the current node is not null.

				Console.Write( current.letter ); // Prints the letter if true.

				if ( current.left != null ) { // Determines if current.left is not null.

					Console.Write( " " ); // Prints a space if true.

					PrintPreOrder( current.left ); // Calls the method itself with current.left as input.

				}

				if ( current.right != null ) { // Determines if current.right is not null.

					Console.Write( " " ); // Prints a space if true.

					PrintPreOrder( current.right ); // Calls the method itself with current.right as input.

				}

			}

		}

		/// <summary>
		/// Wrapper function for the PrintPreOrder method.
		/// </summary>
		public void PrintPreOrder() {

			PrintPreOrder( root ); // Calls the PrintPreOrder method with root as input.

		}

	}

}