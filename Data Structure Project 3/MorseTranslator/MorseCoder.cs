﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace DataStructureProject3.MorseTranslator {

	public class MorseCoder {

		/// <summary>
		/// Private class to ensure no one else will use it.
		/// </summary>
		private class StringLengthComparer : IComparer<string> {
			/// <summary>
			/// Compares 2 string and returns the difference.
			/// </summary>
			/// <param name="x">The string to compare to.</param>
			/// <param name="y">The string to compare against.</param>
			/// <returns>How different they are.</returns>
			public int Compare( string x, string y ) {
				return x.Length - y.Length; // Subtracts their lengths.
			}
		}

		/// <summary>
		/// Gets the singleton.
		/// </summary>
		/// <value>The singleton.</value>
		public static MorseCoder Singleton {

			get {

				if ( singleton is null ) { // Checks to see if singleton is null.

					singleton = new MorseCoder(); // Creates a new MorseCoder and assigns it to a singleton.

				}

				return singleton; // Return singleton.

			}

		}

		private static MorseCoder singleton; // The singleton.

		private Tree morseTree = Tree.Singleton; // Tree singleton.

		private Map map = Map.Singleton; // Map singleton.

		private MorseCoder() { // MorseCoder constructor

			int i; // Variable used in loop to build map and tree.

			int arraySize; // Size of the array that contains all the morse data.

			string[] morseData; // The array that holds all the morse data.

			char letter; // Each alphabetical letter.

			string code; // The morse code with each letter.

			morseTree.Add( '\u0000', "" ); // The creation of the root node of the morse tree.

			// Need the sorted for this loop to populate the tree and map.
			morseData = GrabMorseData();

			arraySize = morseData.Length; // Calculates the number of elements of the morse data array.

			for ( i = 0; i < arraySize; i++ ) { // Loop that builds the morse tree and encode map.

				letter = morseData[i][0]; // Gets each alphabetical letter.

				code = morseData[i].Substring( 1 ); // Gets the morse code.

				morseTree.Add( letter, code ); // Adds each letter to the tree.

				map.AddToMap( morseData[i][0], code ); // Adds each letter and its code to the map where the letter is the key.
			}

		}

		private string[] GrabMorseData() {
			List<string> lines = new List<string>(); // Creates a new list that will store each line.
			Assembly assembly = Assembly.GetAssembly( GetType() ); // Grabs the assembly it is in.
			Stream assemblyStream = assembly.GetManifestResourceStream( "DataStructureProject3.Data.morse.txt" ); // Grabs a stream for reading the embedded file.
			StreamReader assemblyReader = new StreamReader( assemblyStream ); // Creates a stream reader for grabbing the strings.

			using ( assemblyStream ) { // Will auto-close the stream once it leaves scope.
				using ( assemblyReader ) { // Will auto-close the stream once it leaves scope.
					while ( !assemblyReader.EndOfStream ) { // Loops until it has hit the end of the stream.
						string line = assemblyReader.ReadLine(); // Grabs the line from the stream.

						lines.Add( line ); // Adds the line to the list.
					}
				}
			}

			lines.Sort( new StringLengthComparer() ); // Sorts the list by length.

			return lines.ToArray(); // Returns the list array.
		}

		/// <summary>
		/// Prints the tree in pre order.
		/// </summary>
		public void PrintTree() { // Prints the morse tree in Pre Order.

			morseTree.PrintPreOrder(); // Calls the tree method thats prints the tree.

		}

		/// <summary>
		/// Decodes the input morser code.
		/// </summary>
		/// <param name="code">Code.</param>
		public void Decode( string code ) {

			int codeLength; // The length of the morse code string.

			int i; // Variable used in the loop.

			string message; // The message string that will be outputed by the method.

			string letterCode; // Each letter morse code string.

			codeLength = code.Length; // Calculates the length of the input morse code.

			message = ""; // Initializes the message string.

			letterCode = ""; // Initializes the letter code string.


			if ( code[0] != '.' && code[0] != '_' ) { // Test that determines if the first character of the code string is invalid.

				throw new Exception( "Invalid morse code input" ); // Throws exception if true.

			}

			for ( i = 0; i < codeLength; i++ ) { // Loop that decodes each morse code segmant.

				if ( code[i] != '.' && code[i] != '_' && !char.IsWhiteSpace( code[i] ) ) { // Test that determines if the character is invalid.

					throw new Exception( "Invlaid morse code input" ); // Throws exception if true.

				}

				if ( code[i] == '.' || code[i] == '_' ) { // Determies if the character is a "." or a "_".

					letterCode = letterCode + char.ToString( code[i] ); // This adds the segmant to the letter code string that when finshed will be the entire code for one letter.

				}

				if ( char.IsWhiteSpace( code[i] ) || i == ( codeLength - 1 ) ) { // Determines if the character is white space.

					message = message + morseTree.GetNode( letterCode ); // This puts the letter string into the GetNode method from the tree class that will use the morse tree to convert into the message.

					letterCode = ""; // Makes the lettercode string empty again for the next letter code segmant.

				}

			}

			Console.WriteLine( message ); // Outputs the final decoded string.

		}

		/// <summary>
		/// Encodes the input message.
		/// </summary>
		/// <param name="message">Message.</param>
		public void Encode( string message ) {

			int messageLength; // Length of the input message.

			int i; // Variable used in the loop.

			string code; // The output morse code string.

			messageLength = message.Length; // Calculates the length of the message input string.

			code = ""; // Initilizes the code string.

			for ( i = 0; i < messageLength; i++ ) { // Loop that coverts the message string into a morse code string.

				code = code + map.GetCode( message[i] ); // This uses the map to get the morse code of the input letter and adds it to the code string.

				if ( i != messageLength - 1 ) { // This determies if the loop has reached the end of the meesage string.

					code = code + " "; // If it has not it will add a space to the string.

				}

			}

			Console.WriteLine( code ); // Outputs the code string.

		}

	}

}