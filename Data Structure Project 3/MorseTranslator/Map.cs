﻿using System;
using System.Collections.Generic;

namespace DataStructureProject3.MorseTranslator {

	public class Map {

		public static Map Singleton {

			get {

				if ( singleton is null ) { // Checks to see if singleton is null.

					singleton = new Map(); // Creates a new Map and assigns it to a singleton.

				}

				return singleton; // Return singleton.

			}

		}

		private static Map singleton; // The Map singleton

		private Dictionary<char, string> map; // A Map (dictionary in C#) of character keys and String values.

		private Map() { // The private constructor.

			map = new Dictionary<char, string>(); // Creates the map.

		}

		/// <summary>
		/// Adds Key value pair to the map.
		/// </summary>
		/// <param name="letter">Letter.</param>
		/// <param name="code">Code.</param>
		public void AddToMap( char letter, string code ) {

			map.Add( letter, code ); // Adds the letter (key) code (value) pair to the map.

		}

		/// <summary>
		/// Gets the morse code from input letter.
		/// </summary>
		/// <returns>The code.</returns>
		/// <param name="letter">Letter.</param>
		public string GetCode( char letter ) {

			if ( !char.IsLetter( letter ) ) { // Test that determines if the input is valid.

				throw new Exception( "Invalid message" ); // Throws exception if true.

			}

			if ( char.IsUpper( letter ) ) { // Determines if the letter is uppercase.

				letter = char.ToLower( letter ); // Converts the letter to lowercase if true.

			}

			return map[letter]; // Gets the morse code value using the map and letter key.

		}

	}

}