﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructureProject3.MorseTranslator;

namespace Runnable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("==TEST PROGRAM FOR ENCODING AND DECODING MORSE CODE==");
            
            char key = '0';
            string text = "";//temporary variable for use inside the case statements
            bool debugError = false;//whether or not to output extra exception info for invalid inputs
            while (key != 'q')//white not quit
            {

                //output menu items
                Console.WriteLine("");
                Console.WriteLine("q: quit");
                Console.WriteLine("e: encode text into morse code");
                Console.WriteLine("d: decode morse code into text");
                Console.WriteLine("i: toggle error debug info on/off");

                key = Console.ReadKey().KeyChar;//read key choice
                
                switch (key)
                {
                    case 'e':
                        //encrypt text to morse code
                        Console.Write("\nEnter text to encode to morse: ");
                        text = Console.ReadLine();
                        try
                        {
                            MorseCoder.Singleton.Encode(text);//encrypt and output
                        } catch (Exception e)
                        {
                            Console.WriteLine(debugError ? (string)e.ToString() : e.Message);//output error message based on the debug variable
                        }
                        break;
                    case 'd':
                        //decrypt morse code to text
                        Console.Write("\nEnter morse to decode to text: ");
                        text = Console.ReadLine();
                        try
                        {  
                            MorseCoder.Singleton.Decode(text);//decrypt and output

                        } catch (Exception e)
                        {
                            Console.WriteLine(debugError ? (string)e.ToString() : e.Message);//output error message based on the debug variable
                        }
                        break;
                    case 'i':
                        debugError = !debugError;
                        Console.WriteLine("\nExtra Error Info: " + (debugError ? "On" : "Off"));//output toggled error message type
                        break;
                    case 'q':
                        //Console.WriteLine("bye");
                        break;
                    default:
                        Console.WriteLine("\nInvalid choice");//wrong choice
                        break;

                }
            }
            Console.WriteLine("\nPress any key to exit");
            Console.ReadKey();//wait until key press to fully exit
        }
    }
}
